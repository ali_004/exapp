import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import ScreenOneScreen from './screens/ScreenOne';
import ScreenTwoScreen from './screens/ScreenTwo';

const Stack = createStackNavigator();

export default class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="ScreenOne"
            component={ScreenOneScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="ScreenTwo"
            component={ScreenTwoScreen}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
