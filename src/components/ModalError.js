import React, {Component} from 'react';
import {Button, Text, View} from 'react-native';
import Modal from 'react-native-modal';
import styled from 'styled-components';

const ModalError = (props) => (
  <CsModal isVisible={props.display}>
    <Wrapper>
      <Text>Hello!</Text>
      <Button title="Hide modal" onPress={props.onChange} />
    </Wrapper>
  </CsModal>
);

const CsModal = styled(Modal)`
  margin: 0;
`;

const Wrapper = styled(View)`
  width: 100%;
  height: 306px;
  background-color: white;
  margin-top: 100%;
`;

export default ModalError;
