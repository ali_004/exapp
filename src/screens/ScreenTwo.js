import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';

export default class ScreenTwo extends Component {
  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>Successfully</Text>
        <Button
          title="Go to Home"
          onPress={() => this.props.navigation.goBack('ScreenOne')}
        />
      </View>
    );
  }
}
