import React, {Component} from 'react';
import {
  Container as customContainer,
  Header as customHeader,
  Body,
  Title as customTitle,
  Left,
  Content as customContent,
  Form as customForm,
  Label as customLabel,
  Button as customButton,
  Text as txtCustom,
} from 'native-base';
import {View, TextInput} from 'react-native';
import styled from 'styled-components';
import ModalError from '../components/ModalError';

export default class ScreenOne extends Component {
  state = {
    isModalVisible: false,
  };

  toggleModal = () => {
    this.setState({isModalVisible: !this.state.isModalVisible});
  };

  render() {
    return (
      <Container>
        <ModalError
          display={this.state.isModalVisible}
          onChange={this.toggleModal}
        />
        <Header>
          <Left />
          <Body>
            <Title>Learn React Native</Title>
          </Body>
        </Header>
        <Content>
          <Form>
            <Item>
              <Label>Email</Label>
              <Input />
            </Item>
            <Item>
              <Label>Password</Label>
              <Input />
              <TxtForgotPassword>Forgot Password ?</TxtForgotPassword>
            </Item>
            <Button>
              <Text>Login</Text>
            </Button>
            <Button onPress={this.toggleModal}>
              <Text>Click Error</Text>
            </Button>
            <Button
              onPress={() => this.props.navigation.push('ScreenTwo')}
              title="Screen">
              <Text>Click Success</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

const Container = styled(customContainer)`
  border: 0;
`;

const Content = styled(customContent)`
  margin: 0 16px;
`;

const Header = styled(customHeader)`
  background-color: white;
`;

const Title = styled(customTitle)`
  font-family: Poppins-SemiBold;
  color: black;
`;

const Form = styled(customForm)`
  margin-top: 20px;
`;

const TxtForgotPassword = styled(txtCustom)`
  font-family: Poppins-Light;
  font-size: 14px;
  color: #3c4ebb;
`;

const Item = styled(View)`
  border: none;
`;

const Label = styled(customLabel)`
  font-family: Poppins-SemiBold;
  font-size: 15px;
`;

const Input = styled(TextInput)`
  background-color: #ebedf8;
  width: 100%;
  height: 45px;
  border-radius: 4px;
  margin: 10px 0;
`;

const Button = styled(customButton)`
  background-color: #3c4ebb;
  height: 42px;
  margin-top: 10px;
`;

const Text = styled(txtCustom)`
  font-family: Poppins-SemiBold;
  font-size: 14px;
  color: white;
  margin: 0 auto;
`;
